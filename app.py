from flask import Flask, flash, redirect, render_template, request, session, abort
 
app = Flask(__name__)
 
@app.route("/")
def index():
    return "Flask App! Visit <a href='http://127.0.0.1:8080/hello/Python/'>http://127.0.0.1/hello/Python/</a> "
 
@app.route("/hello/<string:name>/")
def hello(name):
    return render_template(
        'index.html',name=name)
 
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
